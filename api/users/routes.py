from typing import Optional

from pydantic import BaseModel, constr, Field
from flask_openapi3 import APIBlueprint
from sqlalchemy import select, update

from database import db
from api.users.models import Users

users_app = APIBlueprint("users_app", __name__)


class UserSchema(BaseModel):
    id: Optional[int]
    password: Optional[str]
    email: Optional[str]
    created_at: Optional[str]
    updated_at: Optional[str]
    last_login: Optional[str]
    first_name: Optional[constr(min_length=1)]
    last_name: Optional[constr(min_length=1)]

    class Config:
        orm_mode = True


class UserPath(BaseModel):
    uid: int = Field(..., description='user id')


class UserList(BaseModel):
    users: list[UserSchema]


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        response = [UserSchema.from_orm(user).dict() for user in users]
        return response


@users_app.get("/users/<int:uid>", responses={"200": UserSchema})
def get_user(path: UserPath):
    with db.session() as session:
        user = session.execute(select(Users).where(Users.id == path.uid)).scalars().first()
        if not user:
            return {"message": f"Not found user {path.uid}"}, 404

        response = UserSchema.from_orm(user).dict()
        return response


@users_app.post("/users", responses={"200": UserSchema})
def create_user(body: UserSchema):
    try:
        with db.session() as session:
            user = Users(**body.dict())
            session.add(user)
            session.commit()
            response = UserSchema.from_orm(user).dict()
            return response
    except Exception as e:
        return {"message": str(e)}, 500


@users_app.patch("/users/<int:uid>/", responses={"200": UserSchema})
def update_user(path: UserPath, body: UserSchema):
    try:
        user = body.dict(exclude_unset=True)
        with db.session() as session:
            stmt = (
                update(Users).
                where(Users.id == path.uid).
                values(**user)
            )
            session.execute(stmt)
            session.commit()
            return {"message": f"user updated {path.uid}"}, 200
    except SomeExceptionAlembic:
        return {"message": "user not found"}, 404

    except Exception as e:
        return {"message": str(e)}, 500
