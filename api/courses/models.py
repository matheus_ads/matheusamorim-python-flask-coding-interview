from sqlalchemy.orm import Mapped, mapped_column

from api.db import Base


class Course(Base):
    __tablename__ = "course"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    max_students: Mapped[int] = mapped_column(nullable=False)
    num_credits: Mapped[int] = mapped_column(nullable=False)
    professor_name: Mapped[int] = mapped_column(nullable=False)
