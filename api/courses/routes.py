from pydantic import BaseModel
from typing import Optional
from flask_openapi3 import APIBlueprint

from api.courses.models import Course
from database import db

courses_app = APIBlueprint("courses_app", __name__)


class CourseSchema(BaseModel):
    id: Optional[int]
    max_students: int
    num_credits: int
    professor_name: str

    class Config:
        orm_mode = True


@courses_app.post("/courses/", responses={"200": CourseSchema})
def create_course(body: CourseSchema):
    try:
        with db.session() as session:
            course = Course(**body.dict())
            session.add(course)
            session.commit()
            return CourseSchema.from_orm(course).dict()
    except Exception as e:
        return {"message": str(e)}, 500
