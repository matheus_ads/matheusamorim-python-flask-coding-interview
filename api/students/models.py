from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from api.db import Base


class Students(Base):
    __tablename__ = "students"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"), nullable=False)
    min_course_credits: Mapped[int] = mapped_column(nullable=False)
    enrollment_date: Mapped[str] = mapped_column(nullable=False)

    user = relationship("Users", back_populates="students")
