from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from api.db import Base


class Enrollment(Base):
    __tablename__ = "enrollment"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    student_id: Mapped[int] = mapped_column(ForeignKey("student.id"), nullable=False)
    course_id: Mapped[int] = mapped_column(ForeignKey("course.id"), nullable=False)
    enrollment_date: Mapped[str] = mapped_column(nullable=False)

    student = relationship("Student", back_populates="enrollment")
    course = relationship("Course", back_populates="enrollment")
