"""create tables courses, students, enrollment

Revision ID: 6bd756a92acc
Revises: 1ddaa1d3a0b7
Create Date: 2024-01-17 10:53:25.443983

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6bd756a92acc'
down_revision = '1ddaa1d3a0b7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
