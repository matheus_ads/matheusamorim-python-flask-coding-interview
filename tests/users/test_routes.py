from api.users.models import Users


def test_get_users_list(client, db):
    user = Users(
        password="test",
        email="test@email.com",
        created_at="2021-01-01",
        updated_at="2021-01-01",
        first_name="test",
        last_name="test",
    )
    with db() as session:
        session.add(user)
        session.commit()

    response = client.get('/users')
    data = response.json
    assert len(data) == 1
